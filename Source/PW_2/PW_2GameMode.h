// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PW_2GameMode.generated.h"

UCLASS(minimalapi)
class APW_2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APW_2GameMode();
};



