// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PW_2GameMode.h"
#include "PW_2Character.h"
#include "UObject/ConstructorHelpers.h"

APW_2GameMode::APW_2GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
